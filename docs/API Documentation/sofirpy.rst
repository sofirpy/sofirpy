.. _API Documentation:

Packages Documentation
======================

Subpackages
-----------

.. toctree::
    :maxdepth: 1

    sofirpy.fmu_export
    sofirpy.simulation
    sofirpy.rdm
